#NiRecor (Intelligent Predictor)

---

## About NiRecor

NiRecor is a part of [Leri Analytics](https://godzilla.uchicago.edu/pages/ngaam/leri).

---

## How to compile

`./build_nirecor`

`./nirecor`

You can change the Search Space and Target Problem in “nirecor_search_space.h” and “nirecor_objective_func.h”, respectively.

DO NOT change anything else if you are NOT sure what functions of the settings.
