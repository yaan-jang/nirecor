/**
 * The part C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

void nirecor_search_space(double **ss, int dim) {
  for(int j = 0; j < dim; j++) {
    ss[0][j] = -100.0;
    ss[1][j] =  100.0;
  }
}
