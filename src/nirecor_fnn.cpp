/**
 * The part C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

void nirecor_fnn(double *out, double x[], int Nfs, vector <double> & wght, vector <double> & bias) {
  double h[HLN], htmp, otmp;
  double o[NOP];

  for(int i = 0; i < HLN; i++) {  
    h[i] = 0.0;
    htmp = bias[i];
    for(int j = 0; j < Nfs; j++) {
      htmp += x[j]*wght[j+i*HLN];
    }
    h[i] = 1.0 / (1.0 + exp(-htmp));    
  }

  for(int k = 0; k < NOP; k++) {  
    o[k] = 0.0;
    otmp = bias[k+HLN];
    for(int i = 0; i < HLN; i++) {
      otmp += h[i] * wght[Nfs*HLN + i + k*HLN];
    }
    o[k] = 1.0 / (1.0 + exp(-otmp));
    out[k] = o[k];
  }
  // for(int k = 0; k < NOP; k++) out[k] = o[k];
}
