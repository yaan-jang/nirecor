/**
 * The part C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

void nirecor_loop(double *opt, int pvect[], double **dat_mat) {
  double rat[2];                              // Prediction percision
  long rnd_uni_init = -(long)pvect[2];        // Initialization of rnd_uni()

  int dim = (pvect[1] + NOP)*HLN + HLN + NOP; // Dimension of an objective function
  double **ss = NULL;                         // Search space
  ss = create2darray(2, dim);
  nirecor_search_space(ss, dim);
  
  

  double iwt = 0.0;                          // Inertia weight
  double acc1 = acc,                         // Acceleration factors
         acc2 = acc;

  bool stop = true;                          // Flag for stop loop
  int cnt = 0;                               // Iteration number (time step)
  double gamma, gamma_1, gamma_2;            // Fitness trackers
  
  int rfc = 0, RFC = 5;                      // Negative operator

  double rmin = 0.0, rmax = 0.0;                       // Lower and upper boundaries
  vector <double> mv (dim);                            // Limits of velocity
  // Definitions of the four populations
  double **pos_a  = NULL, **velocity_a = NULL, **localpos_a = NULL, **global_a = NULL;
  double **pos_b  = NULL, **velocity_b = NULL, **localpos_b = NULL, **global_b = NULL;
  double **pos_c  = NULL, **velocity_c = NULL, **localpos_c = NULL, **global_c = NULL;
  double **pos_d  = NULL, **velocity_d = NULL, **localpos_d = NULL, **global_d = NULL;
  double **global = NULL;

  pos_a = create2darray(SNP, dim+1);
  pos_b = create2darray(SNP, dim+1);
  pos_c = create2darray(SNP, dim+1);
  pos_d = create2darray(SNP, dim+1);

  velocity_a = create2darray(SNP, dim);
  velocity_b = create2darray(SNP, dim);
  velocity_c = create2darray(SNP, dim);
  velocity_d = create2darray(SNP, dim);

  localpos_a = create2darray(SNP, dim+1);
  localpos_b = create2darray(SNP, dim+1);
  localpos_c = create2darray(SNP, dim+1);
  localpos_d = create2darray(SNP, dim+1);

  global_a = create2darray(1, dim+1);
  global_b = create2darray(1, dim+1);
  global_c = create2darray(1, dim+1);
  global_d = create2darray(1, dim+1);

  global = create2darray(1, dim+1);

  // Initialization of global information
  for(int j = 0; j < dim; j++) {
    rmin = ss[0][j];
    rmax = ss[1][j];
    global_a[0][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    global_b[0][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    global_c[0][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    global_d[0][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    global[0][j]   = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
  }

  nirecor_obj(&global_a[0][dim], global_a[0], pvect, dat_mat);
  nirecor_obj(&global_b[0][dim], global_b[0], pvect, dat_mat);
  nirecor_obj(&global_c[0][dim], global_c[0], pvect, dat_mat);
  nirecor_obj(&global_d[0][dim], global_d[0], pvect, dat_mat);
  nirecor_obj(&global[0][dim],   global[0], pvect, dat_mat);

  // Initializations of positions and velocities  
  for (int i = 0; i < SNP; i++) {
    for(int j = 0; j < dim; j++) {
      rmin = ss[0][j];
      rmax = ss[1][j];
      mv[j] = vel_limt * (rmax - rmin);
      
      pos_a[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      pos_b[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      pos_c[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      pos_d[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);

      velocity_a[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      velocity_b[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      velocity_c[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      velocity_d[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);

      localpos_a[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      localpos_b[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      localpos_c[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
      localpos_d[i][j] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    }  
    pos_a[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    pos_b[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    pos_c[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    pos_d[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);

    localpos_a[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    localpos_b[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    localpos_c[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);
    localpos_d[i][dim] = rmin + (rmax - rmin) * rnd_uni(&rnd_uni_init);

    // Evaluations of populations
    nirecor_obj(&pos_a[i][dim], pos_a[i], pvect, dat_mat);
    nirecor_obj(&pos_b[i][dim], pos_b[i], pvect, dat_mat);
    nirecor_obj(&pos_c[i][dim], pos_c[i], pvect, dat_mat);
    nirecor_obj(&pos_d[i][dim], pos_d[i], pvect, dat_mat);
    // Evaluations of local best records
    nirecor_obj(&localpos_a[i][dim], localpos_a[i], pvect, dat_mat);
    nirecor_obj(&localpos_b[i][dim], localpos_b[i], pvect, dat_mat);
    nirecor_obj(&localpos_c[i][dim], localpos_c[i], pvect, dat_mat);
    nirecor_obj(&localpos_d[i][dim], localpos_d[i], pvect, dat_mat);
    // Local best selections
    selection(localpos_a[i], pos_a[i], dim);
    selection(localpos_b[i], pos_b[i], dim);
    selection(localpos_c[i], pos_c[i], dim);
    selection(localpos_d[i], pos_d[i], dim);
    // Global best selections
    selection(global_a[0], pos_a[i], dim);
    selection(global_b[0], pos_b[i], dim);
    selection(global_c[0], pos_c[i], dim);
    selection(global_d[0], pos_d[i], dim);
  }
  selection(global[0], global_a[0], dim);
  selection(global[0], global_b[0], dim);
  selection(global[0], global_c[0], dim);
  selection(global[0], global_d[0], dim);
  double dis_a[SNP], dis_c[SNP];
  double sum_a, sum_c;
  double dg_a, dg_c;
  double dmax_a, dmin_a, dmax_c, dmin_c;
  double f_a, f_c;
  int bidx_a, bidx_c;
  for (int k = 0; k < SNP; k++) {
    dis_a[k] = 0.0;
    dis_c[k] = 0.0;
    for (int i = 0; i < SNP; i++) {
      if (i != k) {
        sum_a = 0.0;
        sum_c = 0.0;
        for (int j = 0; j < dim; j++) {
          sum_a = sum_a + (pos_a[k][j] - pos_a[i][j])*(pos_a[k][j] - pos_a[i][j]);           
          sum_c = sum_c + (pos_c[k][j] - pos_c[i][j])*(pos_c[k][j] - pos_c[i][j]);
        }
        dis_a[k] = dis_a[k] + sqrt(sum_a);
        dis_c[k] = dis_c[k] + sqrt(sum_c);
      }
    }
    dis_a[k] = dis_a[k] / (SNP - 1);
    dis_c[k] = dis_c[k] / (SNP - 1);
  }
  for(int k = 1; k < SNP; k++) {
    if(pos_a[k-1][dim] > pos_a[k][dim]) bidx_a = k;
    if(pos_c[k-1][dim] > pos_c[k][dim]) bidx_c = k;
  }


  dg_a = dis_a[bidx_a];
  dmax_a = *max_element(dis_a, dis_a + SNP);
  dmin_a = *min_element(dis_a, dis_a + SNP);
  f_a = (dg_a - dmin_a) / (dmax_a - dmin_a);

  dg_c = dis_c[bidx_c];
  dmax_c = *max_element(dis_c, dis_c + SNP);
  dmin_c = *min_element(dis_c, dis_c + SNP);
  f_c = (dg_c - dmin_c) / (dmax_c - dmin_c);


  double iwt_a, iwt_c;
  // Main loop
  while (stop == true) {  
    cnt = cnt + 1;
    iwt = iwt_max - (iwt_max - iwt_min) * cnt/NGen;
    iwt_a = 1.0 / (0.5*exp(-2.6*f_a));
    iwt_c = 1.0 / (0.5*exp(-2.6*f_c));
    for(int i = 0; i < SNP; i++) {
      for(int j = 0; j < dim; j++) {
        velocity_a[i][j] =   iwt_a * velocity_a[i][j]
                   + acc1 * (localpos_a[i][j] - pos_a[i][j]) * rnd_uni(&rnd_uni_init)
                   + acc2 * (global[0][j]     - pos_a[i][j]) * rnd_uni(&rnd_uni_init);
        velocity_a[i][j] =   (velocity_a[i][j] >  mv[j]) * mv[j]
                   + (velocity_a[i][j] <= mv[j]) * velocity_a[i][j];
        velocity_a[i][j] =   (velocity_a[i][j] < -mv[j]) * (-mv[j])
                   + (velocity_a[i][j] >=-mv[j]) * velocity_a[i][j];
        pos_a[i][j] = pos_a[i][j] + velocity_a[i][j];

        velocity_b[i][j] =   iwt  * velocity_b[i][j]
                   + acc1 * (localpos_b[i][j] - pos_b[i][j]) * rnd_uni(&rnd_uni_init)
                   + acc2 * (global[0][j]     - pos_b[i][j]) * rnd_uni(&rnd_uni_init);
        velocity_b[i][j] =   (velocity_b[i][j] >  mv[j]) * mv[j]
                   + (velocity_b[i][j] <= mv[j]) * velocity_b[i][j];
        velocity_b[i][j] =   (velocity_b[i][j] < -mv[j]) * (-mv[j])
                   + (velocity_b[i][j] >=-mv[j]) * velocity_b[i][j];
        pos_b[i][j] = pos_b[i][j] + velocity_b[i][j];
      }
      nirecor_obj(&pos_a[i][dim], pos_a[i], pvect, dat_mat);
      nirecor_obj(&pos_b[i][dim], pos_b[i], pvect, dat_mat);

      gamma_1 = pos_a[i][dim];
      gamma_2 = pos_b[i][dim];
      gamma   = gamma_1 + gamma_2;

      for(int j = 0; j < dim; j++) {
        velocity_c[i][j] =   iwt_c * (( gamma * velocity_a[i][j] / gamma_1 
                       + gamma * velocity_b[i][j] / gamma_2) + velocity_c[i][j])
                   + acc1 * (localpos_c[i][j] - pos_c[i][j]) * rnd_uni(&rnd_uni_init)
                   + acc2 * (global[0][j]     - pos_c[i][j]) * rnd_uni(&rnd_uni_init);
        velocity_c[i][j] =   (velocity_c[i][j] >  mv[j]) * mv[j]
                   + (velocity_c[i][j] <= mv[j]) * velocity_c[i][j];
        velocity_c[i][j] =   (velocity_c[i][j] < -mv[j]) * (-mv[j])
                   + (velocity_c[i][j] >=-mv[j]) * velocity_c[i][j];
        pos_c[i][j] = pos_c[i][j] + velocity_c[i][j];
      // }
      // for(int j = 0; j < dim; j++)
      // {
        velocity_d[i][j] =  velocity_a[i][j] + velocity_b[i][j] - velocity_c[i][j];
        velocity_d[i][j] =  (velocity_d[i][j] >  mv[j]) * mv[j]
                   + (velocity_d[i][j] <= mv[j]) * velocity_d[i][j];
        velocity_d[i][j] =  (velocity_d[i][j] < -mv[j]) * (-mv[j])
                  + (velocity_d[i][j] >=-mv[j]) * velocity_d[i][j];
        pos_d[i][j] = pos_d[i][j]/6.0 + localpos_d[i][j]/3.0 + global[0][j]/2.0 + velocity_d[i][j];
      }
      nirecor_obj(&pos_c[i][dim], pos_c[i], pvect, dat_mat);
      nirecor_obj(&pos_d[i][dim], pos_d[i], pvect, dat_mat);

      // Local best selections
      selection(localpos_a[i], pos_a[i], dim);
      selection(localpos_b[i], pos_b[i], dim);
      selection(localpos_c[i], pos_c[i], dim);
      selection(localpos_d[i], pos_d[i], dim);
      // Global best selections
      selection(global_a[0], localpos_a[i], dim);
      selection(global_b[0], localpos_b[i], dim);
      selection(global_c[0], localpos_c[i], dim);
      selection(global_d[0], localpos_d[i], dim);
    }
    selection(global[0], global_a[0], dim);
    selection(global[0], global_b[0], dim);
    selection(global[0], global_c[0], dim);
    selection(global[0], global_d[0], dim);

    // Calculate the distances
    for (int k = 0; k < SNP; k++) {
      dis_a[k] = 0.0;
      dis_c[k] = 0.0; 
      for (int i = 0; i < SNP; i++) {
        if (i != k) {
          sum_a = 0.0;
          sum_c = 0.0; 
          for (int j = 0; j < dim; j++) {
            sum_a = sum_a + (pos_a[k][j] - pos_a[i][j])*(pos_a[k][j] - pos_a[i][j]);
            sum_c = sum_c + (pos_c[k][j] - pos_c[i][j])*(pos_c[k][j] - pos_c[i][j]);
          }
          dis_a[k] = dis_a[k] + sqrt(sum_a);
          dis_c[k] = dis_c[k] + sqrt(sum_c);
        }
      }
      dis_a[k] = dis_a[k] / (SNP - 1);
      dis_c[k] = dis_c[k] / (SNP - 1);
    }
    for(int k = 1; k < SNP; k++) {
      if(pos_a[k-1][dim] > pos_a[k][dim]) bidx_a = k;
      if(pos_c[k-1][dim] > pos_c[k][dim]) bidx_c = k;
    }
    dg_a = dis_a[rand() % SNP];
    dmax_a = *max_element(dis_a, dis_a + SNP);
    dmin_a = *min_element(dis_a, dis_a + SNP);
    f_a = (dg_a - dmin_a) / (dmax_a - dmin_a);

    dg_c = dis_c[rand() % SNP];
    dmax_c = *max_element(dis_c, dis_c + SNP);
    dmin_c = *min_element(dis_c, dis_c + SNP);
    f_c = (dg_c - dmin_c) / (dmax_c - dmin_c);

    rfc = rfc + 1;
    if(rfc >= RFC) {
      int widx_a = 0, widx_b = 0, widx_c = 0, widx_d = 0;
      double wval_a = pos_a[widx_a][dim];
      double wval_b = pos_b[widx_b][dim];
      double wval_c = pos_c[widx_c][dim];
      double wval_d = pos_d[widx_d][dim];
      rfc = 0;
      for(int i = 1; i < SNP; i++) {
        if(pos_a[i][dim] > wval_a) {
          widx_a = i;
          wval_a = pos_a[i][dim];
        }
        if(pos_b[i][dim] > wval_b) {
          widx_b = i;
          wval_b = pos_b[i][dim];
        }
        if(pos_c[i][dim] > wval_c) {
          widx_c = i;
          wval_c = pos_c[i][dim];
        }
        if(pos_d[i][dim] > wval_d) {
          widx_d = i;
          wval_d = pos_d[i][dim];
        }
      }
      for(int j = 0; j< dim; j++) {
        pos_a[widx_a][j] = -global[0][j];
        pos_b[widx_b][j] = -global[0][j];
        pos_c[widx_c][j] = -global[0][j];
        pos_d[widx_d][j] = -global[0][j];
      }
    }
    if (cnt > NGen) stop = false;
  }

  nirecor_cmpt_rate(rat, global[0], pvect, dat_mat);
  printf("Training acc. = %.2f%%, test acc. = %.2f%%.\n", rat[0], rat[1]);
  
  *opt = global[0][dim];

  delete2darray(pos_a, SNP, dim+1);
  delete2darray(pos_b, SNP, dim+1);
  delete2darray(pos_c, SNP, dim+1);
  delete2darray(pos_d, SNP, dim+1);

  delete2darray(velocity_a, SNP, dim+1);
  delete2darray(velocity_b, SNP, dim+1);
  delete2darray(velocity_c, SNP, dim+1);
  delete2darray(velocity_d, SNP, dim+1);

  delete2darray(localpos_a, SNP, dim+1);
  delete2darray(localpos_b, SNP, dim+1);
  delete2darray(localpos_c, SNP, dim+1);
  delete2darray(localpos_d, SNP, dim+1);

  delete2darray(global_a, 1, dim+1);
  delete2darray(global_b, 1, dim+1);
  delete2darray(global_c, 1, dim+1);
  delete2darray(global_d, 1, dim+1);

  delete2darray(global, 1, dim+1); 

  delete2darray(ss, 2, dim);  
}
