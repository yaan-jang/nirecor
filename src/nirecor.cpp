/**
 * The part C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

#include "nirecor.h"
#include "nirecor_search_space.cpp"
#include "nirecor_fnn.cpp"
#include "nirecor_read.cpp"
#include "nirecor_objective_func.cpp"
#include "nirecor_cmpt_rate.cpp"
#include "nirecor_loop.cpp"


void about();

int main() {
  about();                  // Information

  long start, end;
  start = clock();

  string fnam = "Iris.txt";         // Name of input file
  int pvect[5];
  int nsam, nfs;              // Number of samples, number of (feature+label)s
  double **dat_vec = NULL;          // Temporary matrix
  dat_vec = create2darray(SMP_MAX, FSN_MAX);
  nirecor_read(dat_vec, &nsam, &nfs, fnam);
  double **dat_mat = NULL;
  dat_mat = create2darray(nsam, nfs);
  for(int i = 0; i < nsam; i++) {
    for(int j = 0; j < nfs; j++) {
      dat_mat[i][j] = dat_vec[i][j];  
    }
  }
  delete2darray(dat_vec, SMP_MAX, FSN_MAX);

  int seed;                 // Random seed
  int fnum = 0;               // Number of objective functions
  int run, run_max;             // Counter and numbers of trials
  double gopt;                // Best-so-far
  double avg;                 // Average of the best-so-fars 
  run_max = 20;               // Numbers of trials

  pvect[0] = nsam;
  pvect[1] = nfs;

  for(int fidx = fnum; fidx<= fnum; fidx++) {// Loop on problems
    srand((unsigned)time(NULL));      // Initialize seed
    
    avg = 0.0;
    seed = rand() % 100000;         // In the range 0 to 99999;
    for (run = 0; run < run_max; run++) {
      seed += run;
      pvect[2] = seed;
      nirecor_loop(&gopt, pvect, dat_mat);
      avg += gopt;
      // if(run == 0)printf("The %2ist trial, best-so-far: %e\n", run+1, gopt);
      // else if(run == 1)printf("The %2ind trial, best-so-far: %e\n", run+1, gopt);
      // else if(run == 2)printf("The %2ird trial, best-so-far: %e\n", run+1, gopt);
      // else printf("The %2ith trial, best-so-far: %e\n", run+1, gopt);
    }
    avg /= run_max;
    //printf("\nThe average best-so-far of %i trials is: %e\n", run_max, avg);
    
  }
  end = clock();
  printf("Total time used: %f seconds.\n\n", ((double)(end - start)) / CLOCKS_PER_SEC);
  delete2darray(dat_mat, nsam, nfs);
  return 0; 
}// End of main program

/*================== About =============*/
void about() {
  // system("clear");
  printf( "======================================================================\n");
  printf( "|           An Intelligent Predictor (NiRecor)                       |\n");
  printf( "======================================================================\n");
  printf( "|           Author:        N. J. Cheung                              |\n");
  printf( "|           Email:         yaan.jang@gmail.com                       |\n");
  printf( "|           Release:       Version 1.1                               |\n");
  printf( "|           Release Date:  Apr. 14, 2014.                            |\n");
  printf( "|           Last Modified: Jan. 12, 2016.                            |\n");
  printf( "|           http://godzilla.uchicago.edu/pages/ngaam/index.html      |\n");
  printf( "----------------------------------------------------------------------\n");
  printf( "   NiRecor Incentive Product - Leri Executable Build                  \n");
  printf( "   Copyright (C) 2014-   by yaan.jang@gmail.com                       \n");
  printf( "   James Franck Institute, The University of Chicago                  \n");
  printf( "   929 East 57 Street, Chicago, IL 60637.                             \n");
  printf( "\n");
  printf( "   Permission is granted for all academic users and not-for-profit    \n");
  printf( "   institutions, who can copy, use, or modify this software for       \n");
  printf( "   any non-commercial purposes. Commercial users wishing an           \n");
  printf( "   evaluation copy should contact the OWNER. Commercial users MUST    \n");
  printf( "   license this software product after completing the license         \n");
  printf( "   agreement and sending it to yaan.jang@gmail.com. Any other usage   \n");
  printf( "   is specifically prohibited and may constitute a violation of       \n");
  printf( "   United States and international copyright laws.                    \n");
  printf( "\n");
  printf( " References\n");
  printf( " [1] N. J. Cheung, X.-M. Ding, H.-B. Shen.                            \n");
  printf( "     IEEE Transactions on Fuzzy Systems, 22(4): 919-933, 2014.        \n");
  printf( " [2] N. J. Cheung, Z.-K. Xu, X.-M. Ding, H.-B. Shen.                  \n");
  printf( "     European Journal of Operational Research, 247(2): 349-358, 2015. \n");
  printf( " [3] N. J. Cheung, X.-M. Ding, H.-B. Shen.                            \n");
  printf( "     Journal of Computational Chemistry, 37(4): 426-436, 2016.        \n");
  printf( "----------------------------------------------------------------------\n");
  printf("\n");
}

