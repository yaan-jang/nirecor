/**
 * The part C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

void nirecor_read(double **dat_mat, int *nsam, int *fsn, string fnam) {
  string datf, buf, tmp;
  const char* dat_fl;
  datf = "data/" + fnam;
  dat_fl = datf.c_str();  
  int s = 0;
  int k = 0;
  unsigned int i = 0;
  try {
    ifstream fid(dat_fl);
    if(!fid)
      throw ("Cannot open file!");
      // throw strcat((char*)("Cannot open file "), datf);
    while(!fid.eof()) {
      getline(fid, buf);
      for(i = 0; i < buf.size(); i++) {
        tmp += buf.substr(i,1);
        if(buf.substr(i,1) == " " || i+1 == buf.size()) {
          dat_mat[s][k] = atof(tmp.c_str());
          tmp.clear();
          k++;
          if (i+1 == buf.size()) *fsn = k;
        }
      }
      k = 0;
      s++;
    }
    fid.close();
  }
  catch(char* pMsg) { cerr << "\nException:" << pMsg << "\n"; }
  *nsam = s;  
}
