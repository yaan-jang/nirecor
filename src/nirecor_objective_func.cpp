/**
 * The part C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

void nirecor_obj(double *eval, double vec[], int pvect[], double **dat_mat) {
  int Ntr = pvect[0];
  int nfs = pvect[1] - 1;
  int sod = (nfs + NOP)*HLN + HLN + NOP;

  

  double fitness;
  double actualvalue[NOP];  
  // Weight and bias values of neural network
  vector <double> wght ((NOP + nfs)*HLN);
  vector <double> bias (HLN + NOP);


  int m;
  m = (nfs+NOP)*HLN;
  
  for(int i = 0; i < m; i++) {
    wght[i] = vec[i];
//    if(DEBUG) printf("wght[%d]=%f\n", i, wght[i]);
  }

  for(int j = m; j < sod; j++) { 
    bias[j-m] = vec[j];//cout<<"B = "<<b.bias[i-m]<<endl;
  }
  
  
  fitness = 0.0;
  for(int k = 0; k < Ntr; k++) {
    nirecor_fnn(actualvalue, dat_mat[k], nfs, wght, bias);//modify
    if(dat_mat[k][nfs] == 1.0) {
      fitness = fitness+(1.0-actualvalue[0])*(1.0-actualvalue[0]);
      fitness = fitness+(0.0-actualvalue[1])*(0.0-actualvalue[1]);
      fitness = fitness+(0.0-actualvalue[2])*(0.0-actualvalue[2]);
    }
    if(dat_mat[k][nfs] == 2.0) {
      fitness = fitness+(0.0-actualvalue[0])*(0.0-actualvalue[0]);
      fitness = fitness+(1.0-actualvalue[1])*(1.0-actualvalue[1]);
      fitness = fitness+(0.0-actualvalue[2])*(0.0-actualvalue[2]);
    }
    if(dat_mat[k][nfs] == 3.0) {
      fitness = fitness+(0.0-actualvalue[0])*(0.0-actualvalue[0]);
      fitness = fitness+(0.0-actualvalue[1])*(0.0-actualvalue[1]);
      fitness = fitness+(1.0-actualvalue[2])*(1.0-actualvalue[2]);
    }
  }
  
  fitness = fitness / Ntr;
  *eval = fitness;
}
