/**
 * The part C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

void nirecor_cmpt_rate(double rat[], double opt[], int pvect[], double **dat_mat) {
  double actualvalue[NOP];
  int Ntr = pvect[0];
  int nfs = pvect[1] - 1;
  int Sod = (nfs + NOP)*HLN + HLN + NOP;

  // Weight and bias values of neural network
  vector <double> wght ((NOP + nfs)*HLN);
  vector <double> bias (HLN + NOP);


  int TRcnt;//, TEcnt;
  double TRrate = 0.0, TErate = 0.0;

  for(int i = 0; i < (nfs+NOP)*HLN; i++) {
    wght[i] = opt[i];
  }

  for(int i = (nfs+NOP)*HLN; i < Sod; i++) {
    bias[i-(nfs+NOP)*HLN] = opt[i];
  }

  // =============== Calculate the train rate =============== 
  TRcnt = 0; 
  for(int k = 0; k < Ntr; k++) {
    nirecor_fnn(actualvalue, dat_mat[k], nfs, wght, bias);
    if(dat_mat[k][nfs] == 1.0) {
      if(round(actualvalue[0])==1.0&&round(actualvalue[1])==0.0&&round(actualvalue[2])==0.0) {
        TRcnt += 1;
      }
    }
    if(dat_mat[k][nfs] == 2.0) {
      if(round(actualvalue[0])==0.0&&round(actualvalue[1])==1.0&&round(actualvalue[2])==0.0) {
        TRcnt += 1;
      }
    }
    if(dat_mat[k][nfs] == 3.0) {
      if(round(actualvalue[0])==0.0&&round(actualvalue[1])==0.0&&round(actualvalue[2])==1.0) {
        TRcnt += 1;
      }
    }
  }

  TRrate = ((double)TRcnt / (double)Ntr) * 100.0;

  // =============== Calculate the classification rate =============== 
  //TEcnt = 0;

  // cnt.str("");

  rat[0] = TRrate;
  rat[1] = TErate;
}

